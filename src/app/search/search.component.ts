import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SearchResault, Result } from './SearchResault';
import { MdbService } from '../mdb.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  public page: number;
  public query: string;

  public searchResault: SearchResault;
  public results: Result[];

  constructor(
     private route: ActivatedRoute,
     private router: Router,
     private mdb: MdbService
    ) { }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
          this.page = +params['page'];
          this.query = params['query'];
          this.mdb.searchMovie(this.query, this.page.toString()).subscribe(
            (sr: SearchResault) => {
              this.searchResault = sr;
              this.results = sr.results;
            }
          );
      }
  );
  }

  public fullPosterPath(res: Result): string {
    return this.mdb.imageBase + res.poster_path;
  }
  public next() {
    const nextPage = this.page + 1;
    this.router.navigate(['/search', this.query, nextPage.toString()]);
  }

  public previous() {
    const prevPage = this.page - 1;
    this.router.navigate(['/search', this.query, prevPage.toString()]);
  }

  public prevClass(): string {
    if (this.page === 1) {
      return 'page-item disabled';
    } else {
      return 'page-item';
    }
  }
  public nextClass(): string {
    if (this.page === this.searchResault.total_pages) {
      return 'page-item disabled';
    } else {
      return 'page-item';
    }

  }
  public getReleasYear(release_date) {
    return release_date.slice(0, 4);
  }

  public goToMovie(id: number) {
    this.router.navigate([`movie/${id}`]);
  }

  // simple-pagination.js
  // https://gist.github.com/kottenator/9d936eb3e4e3c3e02598
}
