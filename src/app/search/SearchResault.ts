export class Result {
    public poster_path: string;
    public release_date: string;
    public title: string;
    public vote_average: string;
    public id: number;
    public overview: string;
}

export class SearchResault {
    public page: number;
    public results: Result[];
    public total_results: number;
    public total_pages: number;
}
