import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public query: string;
  public selected = 'home';
  constructor(private router: Router, private backend: BackendService) { }

  ngOnInit() {
  }



  public search() {
    this.router.navigate(['/search', this.query, '1']);
  }

  public getClass(name: string) {
    if (name === this.selected) {
            return 'nav-item active';
    } else {
      return 'nav-item';
    }
  }

  public isLogged(): boolean {
    if (this.backend.user) {
      return true;
    }
    return false;
  }

  public logout() {
    this.backend.user = null;
    this.backend.setToken(null);
    this.backend.logout();
  }

  public getFullName(): string {
    return this.backend.user.fullname;
  }

}
