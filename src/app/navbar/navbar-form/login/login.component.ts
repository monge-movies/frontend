import { Component, OnInit } from '@angular/core';
import { BackendService } from '../../../backend.service';
import { Router } from '@angular/router';
declare var jQuery: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public username: string = null;
  public password: string = null;

  public err: string = null;

  constructor(private backend: BackendService, private router: Router) { }

  ngOnInit() {
  }

  public login() {
    this.err = null;
    if (this.validate()) {
      this.backend.authenticate(this.username, this.password).subscribe((res) => {
        if (!res.success) {
          this.err = res.msg;
        } else {
          this.backend.setToken(res.token);
          this.backend.setUser(res.user);
          this.backend.storageSave();
          jQuery('#exampleModal').modal('hide');
        }
      });
    }
  }

  public validate(): boolean {
    const _username: boolean = (this.username !== null && this.username !== '');
    const _passwword: boolean = (this.password !== null && this.password !== '');
    if ((_username && _passwword)) {
      return true;
    } else {
      this.err = 'Fileds Required!';
      return false;
    }
  }

}
