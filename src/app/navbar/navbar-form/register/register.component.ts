import { Component, OnInit } from '@angular/core';
import { BackendService } from '../../../backend.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public username: string = null;
  public password: string = null;
  public email: string = null;
  public fullname: string = null;

  public err: string = null;
  public success: string = null;

  constructor(private backend: BackendService, private router: Router) { }

  ngOnInit() {
  }

  public register() {
    this.err = null;
    this.success = null;
    if (this.validate()) {
      this.backend.register(this.username, this.password, this.email, this.fullname).subscribe((res) => {
        if (!res.success) {
          this.err = res.msg;
        } else {
          this.success = res.msg;
        }
      });
    }
  }

  public validate(): boolean {
    const _username: boolean = (this.username !== null && this.username !== '');
    const _email: boolean = (this.email !== null && this.email !== '');
    const _passwword: boolean = (this.password !== null && this.password !== '');
    const _fullname: boolean = (this.fullname !== null && this.fullname !== '');
    if ((_username && _email && _passwword && _fullname)) {
      return true;
    } else {
      this.err = 'Fileds Required!';
      return false;
    }
  }
}
