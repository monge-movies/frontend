import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { CarouselComponent } from './home/carousel/carousel.component';
import { MostResentComponent } from './home/most-resent/most-resent.component';
import { MovieComponent } from './movie/movie.component';
import { ExtraComponent } from './movie/extra/extra.component';
import { CastComponent } from './movie/extra/cast/cast.component';
import { CrewComponent } from './movie/extra/crew/crew.component';
import { VideosComponent } from './movie/extra/videos/videos.component';
import { SearchComponent } from './search/search.component';
import { PersonComponent } from './person/person.component';
import { PersonExtraComponent } from './person/person-extra/person-extra.component';
import { PersonCastComponent } from './person/person-extra/person-cast/person-cast.component';
import { PersonCrewComponent } from './person/person-extra/person-crew/person-crew.component';
import { RecommendationsComponent } from './movie/recommendations/recommendations.component';
import { UpcomingComponent } from './home/upcoming/upcoming.component';
import { NowPlayingComponent } from './home/now-playing/now-playing.component';
import { NavbarFormComponent } from './navbar/navbar-form/navbar-form.component';
import { LoginComponent } from './navbar/navbar-form/login/login.component';
import { RegisterComponent } from './navbar/navbar-form/register/register.component';
import { TopRatedComponent } from './top-rated/top-rated.component';
import { ReviewsComponent } from './movie/reviews/reviews.component';
import { ReviewFormComponent } from './movie/reviews/review-form/review-form.component';
import { FooterComponent } from './footer/footer.component';

const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'movie/:id',
    component: MovieComponent
  },
  {
     path: 'search/:query/:page',
     component: SearchComponent
  },
  {
    path: 'topRated/:page',
    component: TopRatedComponent
 },
  {
    path: 'person/:id',
    component: PersonComponent
  },
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    CarouselComponent,
    MostResentComponent,
    MovieComponent,
    ExtraComponent,
    CastComponent,
    CrewComponent,
    VideosComponent,
    SearchComponent,
    PersonComponent,
    PersonExtraComponent,
    PersonCastComponent,
    PersonCrewComponent,
    RecommendationsComponent,
    UpcomingComponent,
    NowPlayingComponent,
    NavbarFormComponent,
    LoginComponent,
    RegisterComponent,
    TopRatedComponent,
    ReviewsComponent,
    ReviewFormComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false, useHash: true } // <-- debugging purposes only
    ),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
