import { Component, OnInit, Input } from '@angular/core';
import { Crew } from '../../../Model/Movie/Crew';
import { MdbService } from '../../../mdb.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crew',
  templateUrl: './crew.component.html',
  styleUrls: ['./crew.component.css']
})
export class CrewComponent implements OnInit {

  @Input() crew: Crew[];
  constructor(private mdb: MdbService, private router: Router) { }

  ngOnInit() {
  }

  profilePath(profilePath: string): string {
    return this.mdb.imageBase.replace('w500', 'w138_and_h175_face') + profilePath;
  }

  public toPerson(id) {
    this.router.navigate(['/person', id]);
  }


}
