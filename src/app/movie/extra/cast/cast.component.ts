import { Component, OnInit, Input } from '@angular/core';
import { Cast } from '../../../Model/Movie/Cast';
import { MdbService } from '../../../mdb.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cast',
  templateUrl: './cast.component.html',
  styleUrls: ['./cast.component.css']
})
export class CastComponent implements OnInit {

  @Input() cast: Cast[];
  constructor(private mdb: MdbService, private router: Router) { }

  ngOnInit() {
  }

  profilePath(profilePath: string): string {
    return this.mdb.imageBase.replace('w500', 'w138_and_h175_face') + profilePath;
  }

  public toPerson(id) {
    this.router.navigate(['/person', id]);
  }


}
