import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { Credits } from '../../Model/Movie/Credits';
import { MdbService } from '../../mdb.service';
import { Crew } from '../../Model/Movie/Crew';
import { Cast } from '../../Model/Movie/Cast';

@Component({
  selector: 'app-extra',
  templateUrl: './extra.component.html',
  styleUrls: ['./extra.component.css']
})
export class ExtraComponent implements OnInit, OnChanges {

  constructor(private mdb: MdbService) { }

  public credits: Credits;

  @Input() id;


  ngOnChanges() {
    this.getCredist();
  }

  ngOnInit() {
    this.getCredist();
  }

  public getCredist() {
    this.mdb.getMovieCredits(this.id).subscribe((res) => {
      this.credits = res;
    });
  }
  public getCrew(): Crew[] {
    return this.credits.crew;
  }
  public getCast(): Cast[] {
    return this.credits.cast;
  }
}
