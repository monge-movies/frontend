import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { MdbService } from '../../../mdb.service';
import { Video } from '../../../Model/Movie/video';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.css']
})
export class VideosComponent implements OnInit, OnChanges {
  @Input() id;

  public videos: Video[];

  public ok = false;

  constructor(private mdb: MdbService, private dom: DomSanitizer) { }

  ngOnInit() {
    this.getVideos();
  }
  ngOnChanges() {
    this.getVideos();
  }

  public getVideos() {
    this.mdb.getMovieVideos(this.id).subscribe((vids) => {
      this.videos = vids['results'];
      this.ok = true;
    });
  }

  public getFullLink(link: string): string {
    return `https://www.youtube.com/embed/${link}` ;
  }
}
