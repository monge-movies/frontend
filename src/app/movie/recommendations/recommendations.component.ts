import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { MostPopular } from '../../Model/Movie/MostPopular';
import { MdbService } from '../../mdb.service';
import { Router } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'app-recommendations',
  templateUrl: './recommendations.component.html',
  styleUrls: ['./recommendations.component.css']
})
export class RecommendationsComponent implements OnInit, OnChanges {

  constructor(private mdb: MdbService, private router: Router) { }

  @Input() id;

  public recommendations: MostPopular[];

  ngOnInit() {
    this.getRecommendation();
  }
  ngOnChanges() {
    this.getRecommendation();
  }

  public getRecommendation() {
    this.mdb.getMovieRecomendation(this.id).subscribe(
      res => {
        this.recommendations = res['results'];
        this.recommendations.forEach((mb: MostPopular) => {
          mb.poster_path = this.mdb.imageBase + mb.poster_path;
          mb.release_date = mb.release_date.slice(0, 4);
        });
      });
  }
  public goToMovie(id: number) {
    this.router.navigate([`movie/${id}`]);
  }
}
