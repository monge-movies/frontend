import { Component, OnInit } from '@angular/core';
import { MdbService } from '../mdb.service';
import { Movie } from '../Model/Movie/movie';
import { ActivatedRoute } from '@angular/router';
import { retry } from 'rxjs/operators';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {

  constructor(private mdb: MdbService, private activatedRoute: ActivatedRoute) { }

  public id: string;
  public movie: Movie;

  public loaded = false;

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((route) => {
      this.id = route.get('id');
      this.getMovie(this.id);
    });
  }

  public getMovie(id) {
    this.mdb.getMovie(this.id).subscribe((res: Movie) => {
      this.movie = res;
      this.movie.poster_path = this.mdb.imageBase + this.movie.poster_path;
      this.movie.backdrop_path = this.mdb.imageBase + this.movie.backdrop_path;
      this.movie.backdrop_path = this.movie.backdrop_path.replace('w500', 'original');
      this.loaded = true;
    });
  }

  public getStyle() {
    return {
      'background': `url("${this.movie.backdrop_path}") center center no-repeat`,
      'background-size': 'cover',
      'height': '50vh',
      'overflow' : 'hidden'
    };
  }

  getReleasYear(release_date) {
    return release_date.slice(0, 4);
  }

}
