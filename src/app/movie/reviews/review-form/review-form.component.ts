import { Component, OnInit, Input } from '@angular/core';
import { BackendService } from '../../../backend.service';
import { Router } from '../../../../../node_modules/@angular/router';
declare var jQuery: any;

@Component({
  selector: 'app-review-form',
  templateUrl: './review-form.component.html',
  styleUrls: ['./review-form.component.css']
})
export class ReviewFormComponent implements OnInit {

  @Input() id;

  constructor(private backend: BackendService, private router: Router) { }

  public title: string = null;
  public body: string = null;

  public err: string = null;
  public success: string = null;

  ngOnInit() {
  }

  clickLigon() {
    jQuery('#exampleModal').modal('show');
  }

  public isLogged(): boolean {
    if (this.backend.user) {
      return true;
    }
    return false;
  }

  public validate() {
    const _title: boolean = (this.title !== null && this.title !== '');
    const _body: boolean = (this.body !== null && this.body !== '');

    if (_title && _body) {
      return true;
    } else {
      this.err = 'Values are required!';
      return false;
    }

  }

  public postReview() {
    this.success = null;
    this.err = null;
    if (this.validate()) {
      this.backend.addReview(this.id, this.title, this.body).subscribe((res)  =>  {
        if (res.success) {
          this.success = res.msg;
          this.title = '';
          this.body = '';
          location.reload();
          this.router.navigate([`movie/${this.id}`]);
        } else {
          this.err = res.msg;
        }
      });
    }
  }


}
