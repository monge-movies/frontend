import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { BackendService } from '../../backend.service';
import * as moment from 'moment';
import { Review } from '../../Model/Review/Reviews';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit, OnChanges {

  @Input() id;

  public reviews: Review[] = null;
  constructor(private backend: BackendService) { }

  ngOnChanges() {
    this.getReviews();
  }
  ngOnInit() {
    this.getReviews();
  }
  public format(date: Date): string {
    return moment(date).calendar();
  }

  public getReviews() {
    this.backend.getReviewsByMovieId(this.id).subscribe((result) => {
      this.reviews = result;
    });
  }

}
