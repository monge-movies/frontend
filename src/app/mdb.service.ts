import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MostPopular } from './Model/Movie/MostPopular';
import { Movie } from './Model/Movie/movie';
import { Credits } from './Model/Movie/Credits';
import { SearchResault } from './search/SearchResault';
import { Person } from './Model/Person/Person';
import { PersonCredits } from './Model/Person/Credits';
@Injectable({
  providedIn: 'root'
})
export class MdbService {

  private readonly baseurl = `https://api.themoviedb.org/3/`;

  private readonly apikey = 'api_key=559e21e0278e12a8a88a040c181ef0a2';

  public imageBase = 'https://image.tmdb.org/t/p/w500';

  constructor(private http: HttpClient) { }

  public getMostPopular(): Observable<MostPopular[]> {
    const year = (new Date()).getFullYear;
    const params = new HttpParams();
    params.append('sort_by', 'popularity.desc');
    params.append('primary_release_year', year.toString());
    const reqUrl = `${this.baseurl}discover/movie?${this.apikey}`;
    return this.http.get<MostPopular[]>(reqUrl, {params: params});
  }

  public getMovie(id: string): Observable<Movie> {
    const params = new HttpParams();
    params.append('movie_id', id);
    const reqUrl = `${this.baseurl}movie/${id}?${this.apikey}`;
    return this.http.get<Movie>(reqUrl, {params: params});
  }

  public getMovieCredits(id: string): Observable<Credits> {
    const reqUrl = `${this.baseurl}movie/${id}/credits?${this.apikey}`;
    return this.http.get<Credits>(reqUrl);
  }
  public getTopRated(page: string): Observable<any> { // PutClas
    const reqUrl = `${this.baseurl}movie/top_rated?${this.apikey}&page=${page}`;
    return this.http.get<any>(reqUrl);
  }
  public getMovieVideos(id: string): Observable<any> { // PutClas
    const reqUrl = `${this.baseurl}movie/${id}/videos?${this.apikey}`;
    return this.http.get<any>(reqUrl);
  }

  public getNowPlaying(): Observable<any> { // PutClass
    const reqUrl = `${this.baseurl}movie/now_playing?${this.apikey}`;
    return this.http.get<any>(reqUrl);
  }

  public getPopular(): Observable<any> { // PutClass
    const reqUrl = `${this.baseurl}movie/now_playing?${this.apikey}`;
    return this.http.get<Credits>(reqUrl);
  }

  public getUpComming(): Observable<any> { // PutClas
    const reqUrl = `${this.baseurl}movie/upcoming?${this.apikey}`;
    return this.http.get<Credits>(reqUrl);
  }

  public searchMovie(query: string, page: string): Observable<SearchResault> {
    const reqUrl = `${this.baseurl}search/movie?${this.apikey}&query=${query}&page=${page}`;
    return this.http.get<SearchResault>(reqUrl);
  }

  public getPerson(id: string): Observable<Person> {
    const reqUrl = `${this.baseurl}person/${id}?${this.apikey}`;
    return this.http.get<Person>(reqUrl);
  }

  public getPersonMovieCredits(id: string): Observable<PersonCredits> { // Put Class
    const reqUrl = `${this.baseurl}person/${id}/movie_credits?${this.apikey}`;
    return this.http.get<PersonCredits>(reqUrl);
  }

  public getMovieRecomendation(id: string): Observable<any> {
    const reqUrl = `${this.baseurl}movie/${id}/recommendations?${this.apikey}`;
    return this.http.get<any>(reqUrl);
  }
}
