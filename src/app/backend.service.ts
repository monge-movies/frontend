import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RegisterResponce } from './Model/User/RegisterResponce';
import { AuthenticateResponce, User } from './Model/User/AuthenticateResponce';
import { AddResponce } from './Model/Review/AddResponce';
import { Review } from './Model/Review/Reviews';


@Injectable({
  providedIn: 'root'
})
export class BackendService {

  private readonly url = 'http://localhost:3000';

  private token: string;
  public user: User;

  constructor(private http: HttpClient) {
    if (this.storageLoad()) {
      const token = this.storageLoad();
      this.authenticateToken(token).subscribe((res) => {
        if (res.success) {
          this.user = res.user;
          this.token = token;
        } else {
          localStorage.clear();
        }
      }, (err) => {
        localStorage.clear();
      });
    }

  }

  public setToken(token: string) {
    this.token = token;
  }

  public setUser(user: User) {
    this.user = user;
  }


  public getToken(): string {
      return this.token;
  }

  public register(username: string, password: string, email: string, fullname: string): Observable<RegisterResponce> {
    const url = `${this.url}/users/register`;
    const httpHeaders = new HttpHeaders();
    httpHeaders.append('content-type', 'application/x-www-form-urlencoded');
    const body = {
      username,
      password,
      email,
      fullname
    };
    return this.http.post<RegisterResponce>(url, body, {headers: httpHeaders});
  }

  public authenticate(username: string, password: string): Observable<AuthenticateResponce> {
    const url = `${this.url}/users/authenticate`;
    const httpHeaders = new HttpHeaders();
    httpHeaders.append('content-type', 'application/x-www-form-urlencoded');
    const body = {
      username,
      password,
    };
    return this.http.post<AuthenticateResponce>(url, body, {headers: httpHeaders});
  }

  public authenticateToken(token): Observable<AuthenticateResponce> {
    const url = `${this.url}/users/authenticateToken`;
    const httpHeaders = new HttpHeaders({
      'Content-Type' : 'application/x-www-form-urlencoded',
      'Authorization' : token
    });
    return this.http.get<AuthenticateResponce>(url, {headers: httpHeaders});
  }

  public addReview(movieId, title, body):  Observable<AddResponce> {
    const url = `${this.url}/reviews/add`;
    const httpHeaders = new HttpHeaders({
      'Content-Type' : 'application/json',
      'Authorization' : this.token
    });
    const reqbody = {
      movieId,
      title,
      userID : this.user.id,
      body
    };
    console.log(httpHeaders.get('Authorization'));
    return this.http.post<AddResponce>(url, reqbody, {headers: httpHeaders});
  }

  public getReviewsByMovieId(movieId):  Observable<Review[]> {
    const url = `${this.url}/reviews/${movieId}`;
    const httpHeaders = new HttpHeaders();
    return this.http.get<Review[]>(url, {headers: httpHeaders});
  }

  public storageSave() {
    const obj = {
      token : this.token
    };
    const objStr = JSON.stringify(obj);
    localStorage.setItem('userData', objStr);
  }

  private storageLoad() {
    if (localStorage.getItem('userData') !== null) {
      const objStr = localStorage.getItem('userData');
      const obj = JSON.parse(objStr);
      return obj.token;
    } else {
      return false;
    }
  }

  public logout() {
    localStorage.clear();
  }
}
