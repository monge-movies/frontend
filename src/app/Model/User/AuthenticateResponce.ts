export class User {
    public id: string;
    public username: string;
    public email: string;
    public profileImage: string;
    public fullname: string;
}

export class AuthenticateResponce {
    public success: boolean;
    public token: string;
    public user: User;
    public msg: string;
}
