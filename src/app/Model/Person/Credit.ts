export class Credit {
    public title: string;
    public release_date: string;
    public poster_path: string;
    public id: number;

}
