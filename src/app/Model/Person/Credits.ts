import { Cast } from './Cast';
import { Crew } from './Crew';

export class PersonCredits {
    public id: number;
    public cast: Cast[];
    public crew: Crew[];
}
