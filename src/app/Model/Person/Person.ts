export class Person {
    public name: string;
    public id: number;
    public birthday: string;
    public known_for_department: string;
    public deathday: string;
    public biography: string;
    public place_of_birth: string;
    public profile_path: string;
}
