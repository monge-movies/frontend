export class AddedReview {
    public _id: string;
    public _user: string;
    public movieId: string;
    public title: string;
    public body: string;
    public date: string;
}

export class AddResponce {
    public success: boolean;
    public resault: AddedReview;
    public msg: string;
}
