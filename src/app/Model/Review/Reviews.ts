export class User {
    public _id: string;
    public username: string;
    public fullname: string;
}

export class Review {
    public _id: string;
    public _user: User;
    public movieId: string;
    public title: string;
    public body: string;
    public date: string;
}
