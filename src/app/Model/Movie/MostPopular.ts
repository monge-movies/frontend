export class MostPopular {
    public title: string;
    public vote_average: number;
    public poster_path: string;
    public release_date: string;
    public id: number;

}
