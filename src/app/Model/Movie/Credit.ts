export class Credit {
    public credit_id: string;
    public gender: number;
    public name: string;
    public profile_path: string;
    public id: number;
}
