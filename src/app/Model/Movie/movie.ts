export class Movie {
    public title: string;
    public backdrop_path: string;
    public adult: boolean;
    public genres: Genre[];
    public imdb_id: string;
    public overview: string;
    public poster_path: string;
    public production_companies: ProductionCompanie[];
    public release_date: string;
    public runtime: number;
    public status: string;
    public tagline: string;
    public vote_average: number;
    public id: number;

    public get Genres(): string[] {
        const arr: string[] = [];
        this.genres.forEach((genre: Genre) => {
            arr.push(genre.name);
        });
        return arr;
    }
    public ProductionCompanies(): string[] {
        const arr: string[] = [];
        this.genres.forEach((pc: ProductionCompanie) => {
            arr.push(pc.name);
        });
        return arr;
    }
}

export class Genre {
    public id: number;
    public name: string;
}

export class ProductionCompanie {
    public id: number;
    public logo_path: string;
    public name: string;
    public origin_country: string;
}
