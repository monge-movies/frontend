export class Video {
    public id: string;
    public key: string;
    public name: string;
    public site: string;
    public size: number;
    public type: string;
}
