import { Credit } from './Credit';

export class Cast extends Credit {
    public cast_id: number;
    public character: string;
    public order: number;
}
