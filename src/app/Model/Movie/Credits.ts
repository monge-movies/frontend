import { Cast } from './Cast';
import { Crew } from './Crew';

export class Credits {
    public id: number;
    public cast: Cast[];
    public crew: Crew[];
}
