import { Credit } from './Credit';

export class Crew extends Credit {
    public department: string;
    public job: string;
}
