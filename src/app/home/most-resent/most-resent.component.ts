import { Component, OnInit } from '@angular/core';
import { MdbService } from '../../mdb.service';
import { MostPopular } from '../../Model/Movie/MostPopular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-most-resent',
  templateUrl: './most-resent.component.html',
  styleUrls: ['./most-resent.component.css']
})
export class MostResentComponent implements OnInit {

  public mostPupular: MostPopular[];
  constructor(private mdbService: MdbService, private router: Router) { }

  ngOnInit() {
    this.getMostPupular();
  }

  getMostPupular() {
    this.mdbService.getMostPopular().subscribe((res: MostPopular[]) => {
      this.mostPupular = res['results'];
      console.log(res);
      this.mostPupular.forEach((mb: MostPopular) => {
        mb.poster_path = this.mdbService.imageBase + mb.poster_path;
        mb.release_date = mb.release_date.slice(0, 4);
      });
    });
  }
  public goToMovie(id: number) {
    this.router.navigate([`movie/${id}`]);
  }

}
