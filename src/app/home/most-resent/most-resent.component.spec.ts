import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostResentComponent } from './most-resent.component';

describe('MostResentComponent', () => {
  let component: MostResentComponent;
  let fixture: ComponentFixture<MostResentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostResentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostResentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
