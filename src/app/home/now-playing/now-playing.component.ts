import { Component, OnInit } from '@angular/core';
import { MdbService } from '../../mdb.service';
import { Router } from '../../../../node_modules/@angular/router';
import { Movie } from '../../Model/Movie/movie';

@Component({
  selector: 'app-now-playing',
  templateUrl: './now-playing.component.html',
  styleUrls: ['./now-playing.component.css']
})
export class NowPlayingComponent implements OnInit {


  public nowPlaying: Movie[];
  private excerptLength = 25;
  constructor(private mdbService: MdbService, private router: Router) { }

  ngOnInit() {
    this.getNowPlaying();
  }

  getNowPlaying() {
    this.mdbService.getNowPlaying().subscribe((res: Movie[]) => {
      this.nowPlaying = res['results'];
      this.nowPlaying.forEach((mb: Movie) => {
        mb.poster_path = this.mdbService.imageBase + mb.poster_path;
        mb.release_date = mb.release_date.slice(0, 4);
        mb.backdrop_path = this.mdbService.imageBase + mb.backdrop_path;
        mb.backdrop_path = mb.backdrop_path.replace('w500', 'original');
      });
    });
  }
  public goToMovie(id: number) {
    this.router.navigate([`movie/${id}`]);
  }

  public excerpt(content: string) {
    const str = content.split(' ');
    let count = 0;
    const res = [];
    str.forEach((word) => {
      if (count < this.excerptLength) {
        res.push(word);
        count ++;
      }
    });

    return res.join(' ') + ' ...';
  }

  public getColStyle(m: Movie) {
    const url = m.backdrop_path;
    return {
      'background': `url("${url}") center center no-repeat`,
      'background-size': 'cover',
      'overflow' : 'hidden',
      'cursor': 'pointer'
    };
  }
}
