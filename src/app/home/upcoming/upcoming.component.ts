import { Component, OnInit } from '@angular/core';
import { MostPopular } from '../../Model/Movie/MostPopular';
import { MdbService } from '../../mdb.service';
import { Router } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'app-upcoming',
  templateUrl: './upcoming.component.html',
  styleUrls: ['./upcoming.component.css']
})
export class UpcomingComponent implements OnInit {


  public upComming: MostPopular[];
  constructor(private mdbService: MdbService, private router: Router) { }

  ngOnInit() {
    this.getUpComming();
  }

  getUpComming() {
    this.mdbService.getUpComming().subscribe((res: MostPopular[]) => {
      this.upComming = res['results'];
      this.upComming.shift();
      this.upComming.shift();
      this.upComming.shift();
      this.upComming.forEach((mb: MostPopular) => {
        mb.poster_path = this.mdbService.imageBase + mb.poster_path;
        mb.release_date = mb.release_date.slice(0, 4);
      });
    });
  }
  public goToMovie(id: number) {
    this.router.navigate([`movie/${id}`]);
  }
}
