import { TestBed, inject } from '@angular/core/testing';

import { MdbService } from './mdb.service';

describe('MdbService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MdbService]
    });
  });

  it('should be created', inject([MdbService], (service: MdbService) => {
    expect(service).toBeTruthy();
  }));
});
