import { Component, OnInit } from '@angular/core';
import { MdbService } from '../mdb.service';
import { ActivatedRoute } from '@angular/router';
import { Person } from '../Model/Person/Person';


@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {

  public id: number;
  public person: Person;


  constructor(
    private mdb: MdbService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe((param) => {
      this.id = +param.get('id');
      this.getPerson();
    });
  }

  public getPerson() {
    this.mdb.getPerson(this.id.toString()).subscribe((res) => {
      this.person = res;
    });
  }


  profilePath(profilePath: string): string {
    // .replace('w500', 'w138_and_h175_face')
    return this.mdb.imageBase + profilePath;
  }

}
