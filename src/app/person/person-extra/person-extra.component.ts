import { Component, OnInit, Input } from '@angular/core';
import { PersonCredits } from '../../Model/Person/Credits';
import { Crew } from '../../Model/Person/Crew';
import { Cast } from '../../Model/Person/Cast';
import { MdbService } from '../../mdb.service';


@Component({
  selector: 'app-person-extra',
  templateUrl: './person-extra.component.html',
  styleUrls: ['./person-extra.component.css']
})
export class PersonExtraComponent implements OnInit {
  public personCredits: PersonCredits;

  @Input() id;

  constructor(private mdb: MdbService) { }

  ngOnInit() {
    this.getPersonCredit();
  }

  public getPersonCredit() {
    this.mdb.getPersonMovieCredits(this.id.toString()).subscribe((res) => {
      this.personCredits = res;
    });
  }

  public getCrew(): Crew[] {
    return this.personCredits.crew;
  }

  public getCast(): Cast[] {
    return this.personCredits.cast;
  }

}
