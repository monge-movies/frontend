import { Component, OnInit, Input } from '@angular/core';
import { Crew } from '../../../Model/Person/Crew';
import { MdbService } from '../../../mdb.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-person-crew',
  templateUrl: './person-crew.component.html',
  styleUrls: ['./person-crew.component.css']
})
export class PersonCrewComponent implements OnInit {

  @Input() crew: Crew[];

  constructor(private mdb: MdbService, private router: Router) { }

  ngOnInit() {
  }

  posterPath(profilePath: string): string {
    return this.mdb.imageBase.replace('w500', 'w138_and_h175_face') + profilePath;
  }

  public toMovie(id) {
    this.router.navigate(['/movie', id]);
  }


}
