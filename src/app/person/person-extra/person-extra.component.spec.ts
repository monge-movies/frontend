import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonExtraComponent } from './person-extra.component';

describe('PersonExtraComponent', () => {
  let component: PersonExtraComponent;
  let fixture: ComponentFixture<PersonExtraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonExtraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonExtraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
