import { Component, OnInit, Input } from '@angular/core';
import { Cast } from '../../../Model/Person/Cast';
import { MdbService } from '../../../mdb.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-person-cast',
  templateUrl: './person-cast.component.html',
  styleUrls: ['./person-cast.component.css']
})
export class PersonCastComponent implements OnInit {

  @Input() cast: Cast[];

  constructor(private mdb: MdbService, private router: Router) { }

  ngOnInit() {
  }

  posterPath(profilePath: string): string {
    return this.mdb.imageBase.replace('w500', 'w138_and_h175_face') + profilePath;
  }

  public toMovie(id) {
    this.router.navigate(['/movie', id]);
  }

}
