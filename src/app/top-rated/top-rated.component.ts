import { Component, OnInit } from '@angular/core';
import { SearchResault, Result } from '../search/SearchResault';
import { ActivatedRoute, Router } from '@angular/router';
import { MdbService } from '../mdb.service';

@Component({
  selector: 'app-top-rated',
  templateUrl: './top-rated.component.html',
  styleUrls: ['./top-rated.component.css']
})
export class TopRatedComponent implements OnInit {

  public page: number;

  public searchResault: SearchResault;
  public results: Result[];

  private excerptLength = 10;

  constructor(
     private route: ActivatedRoute,
     private router: Router,
     private mdb: MdbService
    ) { }

  ngOnInit() {
    this.route.params.subscribe(
      params => {
          this.page = +params['page'];
          this.mdb.getTopRated(this.page.toString()).subscribe(
            (sr: SearchResault) => {
              this.searchResault = sr;
              this.results = sr.results;
            }
          );
      }
  );
  }

  public fullPosterPath(res: Result): string {
    return this.mdb.imageBase + res.poster_path;
  }
  public next() {
    const nextPage = this.page + 1;
    this.router.navigate(['/topRated', nextPage.toString()]);
  }

  public previous() {
    const prevPage = this.page - 1;
    this.router.navigate(['/topRated', prevPage.toString()]);
  }

  public prevClass(): string {
    if (this.page === 1) {
      return 'page-item disabled';
    } else {
      return 'page-item';
    }
  }
  public nextClass(): string {
    if (this.page === this.searchResault.total_pages) {
      return 'page-item disabled';
    } else {
      return 'page-item';
    }

  }
  public getReleasYear(release_date) {
    return release_date.slice(0, 4);
  }

  public goToMovie(id: number) {
    this.router.navigate([`movie/${id}`]);
  }
  public excerpt(content: string) {
    const str = content.split(' ');
    let count = 0;
    const res = [];
    str.forEach((word) => {
      if (count < this.excerptLength) {
        res.push(word);
        count ++;
      }
    });

    return res.join(' ') + ' ...';
  }
}
